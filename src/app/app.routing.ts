import { RouterModule, Routes } from '@angular/router';

//importmaos componentes
import {MedicamentosComponent} from './components/medicamentos/medicamentos.component';
import {ProveedoresComponent} from './components/proveedores/proveedores.component';
import {LaboratorioComponent} from './components/laboratorio/laboratorio.component';
import {FormComponent} from './components/medicamentos/form/form.component';

const APP_ROUTES: Routes = [
  { path: 'proveedores', component: ProveedoresComponent },
  { path: 'medicamentos', component: MedicamentosComponent},
  { path: 'form', component: FormComponent},
  { path: 'laboratorio', component: LaboratorioComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'proveedores' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
