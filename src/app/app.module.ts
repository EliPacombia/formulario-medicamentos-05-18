import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

//rutas
import {APP_ROUTING} from './app.routing';


import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { MenuComponent } from './components/menu/menu.component';
import { MedicamentosComponent } from './components/medicamentos/medicamentos.component';
import { LaboratorioComponent } from './components/laboratorio/laboratorio.component';
import { ProveedoresComponent } from './components/proveedores/proveedores.component';
import { FormComponent } from './components/medicamentos/form/form.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MenuComponent,
    MedicamentosComponent,
    LaboratorioComponent,
    ProveedoresComponent,
    FormComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    NgbModule,
    NgbModule.forRoot()

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
