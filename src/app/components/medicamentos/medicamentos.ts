export interface Medicamento{
  nombre:string,
  laboratorio:string,
  alert:number;
  presentacionUni:string,
  presentacion:string,
  cantidad:number,
  codigioBarra:string
}
