import { Component, OnInit } from '@angular/core';
import {Medicamento} from '../medicamentos';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {
  title="Formulario Medicamento"
  medicamentoList:Medicamento[]=[];
  guardar(form){
    var medicamento:Medicamento;
    medicamento=form.value;
    this.medicamentoList.push(medicamento)
    console.log(form.value);
  }
  constructor() { }

  ngOnInit() {
  }

}
