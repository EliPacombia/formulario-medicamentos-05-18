import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-laboratorio',
  templateUrl: './laboratorio.component.html',
  styles:[`
      .ng-invalid.ng-touched:not(form){
        border:1px solid red;
      }
    `]
})
export class LaboratorioComponent  {
  title="Laboratorio";
  usuario:Object={
    nombre:null,
    apellido:null,
    correo:null,
    pais:""
  }
  paises=[
      {
        codigo:"CRI",
        nombre:"Costa Rica"
      },
      {
        codigo:"ESP",
        nombre:"España"
      }
  ]
   page = 1;
  closeResult: string;

  constructor(private modalService: NgbModal) {  }
  guardar(forma:NgForm){
    console.log("formulario Posteado!!!");
    console.log("ngForm",forma);
    console.log("valor forma",forma.value);
    console.log("Usuario",this.usuario);
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
