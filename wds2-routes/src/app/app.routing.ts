import { RouterModule, Routes } from '@angular/router';

//component
import {EjemploComponent} from './components/ejemplo/ejemplo.component';
import {CalendarioComponent} from './components/calendario/calendario.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';


const APP_ROUTES: Routes = [
  { path: 'example', component: EjemploComponent },
  { path: 'calendario', component: CalendarioComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'dashboard' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
