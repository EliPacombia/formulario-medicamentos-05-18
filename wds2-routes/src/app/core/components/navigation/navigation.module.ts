import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';
import {FuseNavigationComponent} from './navigation.component';

@NgModule({
  imports: [
    CommonModule,
   RouterModule
  ],
  declarations: [
    FuseNavigationComponent
  ],
  exports:[
    FuseNavigationComponent
  ]
})
export class FuseNavigationModule { }
