import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//componente
import { FuseSearchBarComponent } from './search-bar.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    FuseSearchBarComponent
  ],
  exports:[
    FuseSearchBarComponent
  ]
})
export class FuseSearchBarModule { }
