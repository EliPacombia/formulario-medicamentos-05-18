import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


//componentes
import {FuseShortcutsComponent} from './shortcuts.component';
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    FuseShortcutsComponent
  ],
  exports:[
    FuseShortcutsComponent
  ]
})
export class FuseShortcutsModule { }
