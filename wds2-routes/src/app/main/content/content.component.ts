import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'fuse-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class FuseContentComponent implements OnInit {
  page = 3;

  constructor() { }

  ngOnInit() {
  }

}
