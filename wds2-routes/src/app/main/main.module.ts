import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterModule} from '@angular/router';

//modulos
import {FuseNavigationModule} from '../core/components/navigation/navigation.module';
import {FuseSearchBarModule} from '../core/components/search-bar/search-bar.module';
import {FuseShortcutsModule} from '../core/components/shortcuts/shortcuts.module';


//componentes
import { FuseMainComponent } from './main.component';
import { FuseVerticalComponent } from './navbar/vertical/vertical.component';
import { FuseHorizontalComponent } from './navbar/horizontal/horizontal.component';
import { FuseContentComponent } from './content/content.component';
import { FuseQuickPanelComponent } from './quick-panel/quick-panel.component';
import { FuseToolbarComponent } from './toolbar/toolbar.component';
import { FuseSampleComponent } from './content/sample/sample.component';


@NgModule({
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    FuseNavigationModule,
    FuseSearchBarModule,
    FuseShortcutsModule,
    RouterModule
  ],
  declarations: [
    FuseVerticalComponent,
    FuseHorizontalComponent,
    FuseQuickPanelComponent,
    FuseToolbarComponent,
    FuseContentComponent,
    FuseMainComponent,
    FuseSampleComponent
  ],
  exports     : [
      FuseMainComponent
  ]
})
export class FuseMainModule { }
