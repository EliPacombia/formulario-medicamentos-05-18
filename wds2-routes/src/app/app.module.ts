import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


//rutas
import {APP_ROUTING} from './app.routing';

//componentes
import { AppComponent } from './app.component';

//module
import {FuseMainModule} from './main/main.module';
import { EjemploComponent } from './components/ejemplo/ejemplo.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { CalendarioComponent } from './components/calendario/calendario.component';

@NgModule({
  declarations: [
    AppComponent,
    EjemploComponent,
    DashboardComponent,
    CalendarioComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    FuseMainModule,
    APP_ROUTING
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
